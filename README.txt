Please make sure this module has been downloaded using composer.

How to install this module?
---------------------------
1. Install this module by drush command - drush en general_layouts
2. Or install by "Extend" menu. (Drupal Admin Backend)

How to use this module?
-----------------------

1. Layout builder and layout discovery module should be enabled to use this
module.
2. This module can also be used with Layout Paragraphs module.

For managing layouts and seeing available layouts, please visit link -
admin/structure/types/manage/page/display/default/layout
